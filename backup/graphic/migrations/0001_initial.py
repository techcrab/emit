# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entrada',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('value', models.FloatField()),
                ('label', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Grafico',
            fields=[
                ('titulo_grafico', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('tipoGrafico', models.CharField(max_length=40)),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
                ('relatorio_ref', models.ForeignKey(to='report.Relatorio')),
            ],
        ),
        migrations.AddField(
            model_name='entrada',
            name='entrada_ref',
            field=models.ForeignKey(to='graphic.Grafico'),
        ),
    ]
