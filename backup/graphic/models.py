from django.db import models
# Create your models here.

class Grafico(models.Model):
	relatorio_ref = models.ForeignKey(Relatorio)
	titulo_grafico = models.CharField(max_length = 100, primary_key=True)
	tipoGrafico = models.CharField(max_length = 40)
	x = models.IntegerField()
	y = models.IntegerField()

	def __str__(self):
		return self.titulo_grafico

class Entrada (models.Model):
	entrada_ref = models.ForeignKey(Grafico)
	value = models.FloatField()
	label = models.CharField(max_length = 40)

	def __str__(self):
		return self.label