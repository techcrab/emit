from django.db import models
from report.models import *

# Create your models here.

class CaixaDeTexto(models.Model):
	relatorio_linkado = models.ForeignKey(Relatorio)
	texto = models.TextField()
	x = models.IntegerField()
	y = models.IntegerField()

	def __str__(self):
		return self.texto