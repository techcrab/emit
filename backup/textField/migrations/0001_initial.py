# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaixaDeTexto',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('texto', models.TextField()),
                ('x', models.IntegerField()),
                ('y', models.IntegerField()),
                ('relatorio_linkado', models.ForeignKey(to='report.Relatorio')),
            ],
        ),
    ]
