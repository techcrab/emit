from django.shortcuts import render
from django.views.generic import CreateView
from .models import Tabela
from .form import TabelaForm
from django.core.urlresolvers import reverse_lazy


class TesteCreateView(CreateView):
	model = Tabela
	template_name = "home.html"
	form_class = TabelaForm
	success_url = reverse_lazy('core:home')

home = TesteCreateView.as_view()