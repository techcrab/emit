from .models import Tabela
from django.forms import ModelForm

class TabelaForm(ModelForm):
    class Meta:
        model = Tabela
        fields = ['n']
