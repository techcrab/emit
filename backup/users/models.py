from django.db import models

# Create your models here.

class Membro(models.Model):
	primeiro_nome = models.CharField(max_length=30)
	ultimo_nome = models.CharField(max_length=30)
	cargo =  models.CharField(max_length=30)
	email = models.EmailField()
	senha = models.CharField(max_length=30)

	def __str__(self):
		return self.primeiro_nome

class Adm(models.Model):
	admin = models.ForeignKey(Membro)
	
	def __str__(self):
		return self.admin.primeiro_nome

class Usuario(models.Model):
	usr = models.ForeignKey(Membro)

	def __str__(self):
		return self.usr.primeiro_nome