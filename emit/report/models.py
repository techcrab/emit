from django.db import models

# Create your models here.

class Tag(models.Model):
	nome_tag = models.CharField(max_length = 30)
		
	def __str__(self):
		return self.nome_tag


class Relatorio(models.Model):
	tag_ref = models.ForeignKey(Tag)
	titulo_relatorio = models.CharField(max_length = 20)

	def __str__(self):
		return self.titulo_relatorio
