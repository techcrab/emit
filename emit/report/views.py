from django.shortcuts import render
from django.http import HttpResponse

def home(request):
	template_name = 'relatorios.html'
	return render(request, template_name)